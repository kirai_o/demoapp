#!/usr/bin/env python
#coding:utf-8
# ライブラリーの定義
import os
import json
import requests
requests=requests.Session()

# 変数設定
key="<APIKey>"
base_url="https://gateway-a.watsonplatform.net/calls/image/ImageGetRankedImageFaceTags"
post_url="%(base_url)s?apikey=%(key)s&imagePostMode=raw&outputMode=json" % locals()

###########################
# main処理
###########################
# 写真撮影
print "Start a photography"
while True:
    print "Please Enter the [y] when you are ready"
    input_line = raw_input()
    if input_line=="y":
        os.system('fswebcam -r 600x480 --no-banner /work/watson/test1.jpg')
        break
    else:
        print "Wrong input word"

#撮影した写真を変数へ格納
test_jpg="/work/watson/test1.jpg"
post_jpg=open(test_jpg, 'rb').read()

#撮影した写真をAlchemy APIへ送付
print "####START####"
print "##POST先のURLを表示"
print post_url
print "##撮影した写真をAPIにPOSTし、実行結果を取得"
result=requests.post(url=post_url, data=post_jpg)
print result.text