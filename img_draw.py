#!/usr/bin/env python
#coding:utf-8
# ライブラリーの定義
import os
import json
import requests
import cv2
requests=requests.Session()

# 変数設定
key="<APIKey>"
base_url="https://gateway-a.watsonplatform.net/calls/image/ImageGetRankedImageFaceTags"
post_url="%(base_url)s?apikey=%(key)s&imagePostMode=raw&outputMode=json" % locals()

###########################
# main処理
###########################
# 撮影した写真を変数へ格納
test_jpg="<写真のファイルパス>"
post_jpg=open(test_jpg, 'rb').read()

# POST先のURLを表示
print "####START####"
print "##POST先のURLを表示"
print post_url

# APIへPOSTし、実行結果のJSONを変数にパース
result=requests.post(url=post_url, data=post_jpg)
text=json.loads(result.text)

# APIの実行結果(Status)を表示
print "##APIの判定結果(Status)を表示"
print 'Execution result='+text['status']

# 撮影した写真をOpenCVで処理するために変数へ格納
img=cv2.imread("<写真のファイルパス>", 1)
# 認識した顔数ぶんの処理を行うため,カウント変数にAPIの実行結果(JSON)を格納
count=text['imageFaces']

print "##実行結果を基に,人の顔に枠と年齢を描画(OpenCVのライブラリを利用)
for cnt in count:
 # APIの実行結果から認識した顔の座標と範囲、年齢、性別情報を変数に格納(座標と範囲は数値に変換)
 x_position=int(cnt['positionX'])
 y_position=int(cnt['positionY'])
 x_width=int(cnt['width'])
 y_height=int(cnt['height'])
 text_age=cnt['age']['ageRange']
 text_gender=cnt['gender']['gender']
 print 'X='+str(x_position)+', width='+str(x_width)+', Y='+str(y_position)+\
          ', height='+str(y_height)+', age='+text_age+', gender='+text_gender

 # pythonのOpenCVライブラリを利用して認識した顔に枠と年齢・性別を描画
 cv2.rectangle(img,(x_position, y_position),(x_position+x_width,y_position+y_height),(0,167,113),2)
 cv2.putText(img, text_age+":"+text_gender,(x_position+x_width,y_position+y_height),cv2.CV_AA,1,(0,167,113),3)

# 保存処理
cv2.imwrite("<別名で保存するためのファイルパス>",img)
cv2.waitKey(0)
cv2.destroyAllWindows()

print "####END####"