#!/usr/bin/python
#coding:utf-8
import os
import base64
import json
from requests import Request, Session

# Cloud Vision APIに送信する写真の撮影
print "Start a photography"
while True:
    print "Please Enter the [y] when you are ready"
    input_line = raw_input()
    if input_line=="y":
        re_code = os.system('fswebcam -r 600x480 --no-banner /work/demo02/test1.jpg')
        break   
    else:
        print "Wrong input word"

# Cloud Vision APIで画像を分析
# 画像の読み込み/base64で画像をエンコード
bin_captcha = open('/work/demo02/test1.jpg', 'rb').read()
str_encode_file = base64.b64encode(bin_captcha)

# APIのURLを指定/APIキーの設定
str_url = "https://vision.googleapis.com/v1/images:annotate?key="
str_api_key = "<APIKey>"

# Content-TypeをJSONに設定
str_headers = {'Content-Type': 'application/json'}

# Cloud Vision APIの仕様に沿ってJSONのペイロードを定義。
# 画像からテキストを抽出するため、typeは「TEXT_DETECTION」にする。
str_json_data = {
   'requests': [
        {
           'image': { 'content': str_encode_file },
                'features': [
                    {
                        'type': "TEXT_DETECTION",
                        'maxResults': 10
                    }
                ]
        }
    ]
}

# リクエスト送信
obj_session = Session()
obj_request = Request("POST",
                    str_url + str_api_key,
                    data=json.dumps(str_json_data),
                    headers=str_headers
                    )
obj_prepped = obj_session.prepare_request(obj_request)
obj_response = obj_session.send(obj_prepped,
                                verify=True,
                                timeout=60
                                )

# 分析結果の取得
if obj_response.status_code == 200:
    print obj_response.text
else:
    print "error"
